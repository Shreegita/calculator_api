
class calculator:
    def addition(self,num1,num2):
        result = num1+num2
        return result

    def division(self,num1,num2):
            try:
                result = num1/num2
                return result
            except ZeroDivisionError:
                return "you cannot divide by zero"

    def multiplication(self,num1,num2):
        result = num1 * num2
        return result

    def substraction(self, num1, num2):
        result = num1 - num2
        return result
