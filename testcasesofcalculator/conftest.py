
import pytest

from calculator import calculator

@pytest.fixture()
def createobj():
    cal = calculator()
    return cal

@pytest.fixture()
def positivenumbers():
    print("sending positive numbers")
    #returning list of numbers
    return [1000,200,50]

@pytest.fixture(params=[(3000,1000,4000),(1,1,2),(40,45,85),(1000,2000,3000)])
def sendpositivenumbers(request):
    #return request.param returns single tuple from params with each iteration
    return request.param