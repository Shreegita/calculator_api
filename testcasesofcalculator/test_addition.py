
import pytest

from calculator import calculator
cal=None


def testadditionwithpositivenumbers(sendpositivenumbers):
    cal = calculator()
    res = cal.addition(sendpositivenumbers[0],sendpositivenumbers[1]);
    assert res == sendpositivenumbers[2]

def testadditionwithpositiveandnegativenumbers(createobj):
    #cal = calculator()
    res = createobj.addition(100,-20);
    assert res == 80

def testadditionwithzeronumbers(createobj):
    #cal = calculator()
    res = createobj.addition(0,0);
    assert res == 0