import pytest

from calculator import calculator

cal=None

def testdivisionwithpositivenumbers(createobj):
    #cal = calculator()
    res = createobj.division(100,20)
    assert res == 5

def testdivisionwithnonzeroandzeronumber(createobj):
    #cal = calculator()
    res = createobj.division(100,0)
    assert res == "you cannot divide by zero"